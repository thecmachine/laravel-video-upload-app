<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class VideoControllerTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testUploadLoggedIn()
    {
	    $this->visit('/upload')
	    	->type('Filename', 'title')
	    	->attach('videos/keep.mp4', 'file')
	    	->press('Upload Video')
	    	->see('Saved Video');
    }
}
