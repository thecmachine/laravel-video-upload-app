<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class BasicTest extends DuskTestCase
{
    /**
     * A basic browser test example.
     *
     * @return void
     */
    public function testHomePage()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/')
                    ->assertSee('Clint\'s WWE Laravel Video App');
        });
    }
    
     /**
     * Test login page
     *
     * @return void
     */
    public function testLoginPage()
    {
	    $this->browse(function (Browser $browser) {  
		    $browser->visit('/')
                    ->clickLink('Login')
                    ->assertSee('Login');
	    });
    }
    
     /**
     * Test Register page
     *
     * @return void
     */
    public function testRegisterPage()
    {
	    $this->browse(function (Browser $browser) {  
		    $browser->visit('/')
                    ->clickLink('Register')
                    ->assertSee('Register');
	    });
    }

    public function testUploadPageUserNotLoggedIn()
    {
	    $this->browse(function (Browser $browser) {  
		    $browser->visit('/upload')
                    ->assertPathIs('/login')
                    ->assertSee('Login');
	    });   
    }
    
    public function testVideoPageUserNotLoggedIn()
    {
	    $this->browse(function (Browser $browser) {  
		    $browser->visit('/videos')
                    ->assertPathIs('/login')
                    ->assertSee('Login');
	    });   
    }
    
    public function testMetadataPageUserNotLoggedIn()
    {
	    $this->browse(function (Browser $browser) {  
		    $browser->visit('/metadata')
                    ->assertPathIs('/login')
                    ->assertSee('Login');
	    });   
    }
    
	public function testUploadPageUserLoggedIn()
    {	
	    $this->browse(function (Browser $browser) {  
		    $user = factory(\App\User::class)->create(); 
		    $browser->loginAs($user->id)
		    		->visit('/upload')
                    ->assertPathIs('/upload')
                    ->assertSee('Upload')
                    ->clickLink('Logout');
	    });   
    }
    
    public function testVideoPageUserLoggedIn()
    {
	    $this->browse(function (Browser $browser) {  
		    $user = factory(\App\User::class)->create();     
		    $browser->loginAs($user->id)
		    		->visit('/videos')
                    ->assertPathIs('/videos')
                    ->assertSee('Videos');
	    });   
    }	

  
    public function testMetadataPageUserLoggedIn()
    {
	    $this->browse(function (Browser $browser) {  
		    $user = factory(\App\User::class)->create();     
		    $browser->loginAs($user->id)
		    		->visit('/metadata')
                    ->assertPathIs('/metadata')
                    ->assertSee('Metadata');
	    });   
    }	
}
