@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <a class="pages" href="/upload">Upload</a>
                    <a class="pages" href="/videos">Videos</a>
                    <a class="pages" href="/metadata">Metadata</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
