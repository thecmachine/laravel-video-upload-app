##  Laravel WWE Video Upload App

* I love the WWE so much I caught grief from the cool kids in middle school, who's laughing now though?!
* Back then we didn't know what Kane's face looked like, and he talked through X-Pac.
* 1998 [Hell in a Cell, Undertaker threw Mankind off the cage](https://streamable.com/3i43q).  That is my favorite moment in wrestling history.
* Listen to JR and Jerry the King lose their composer "oh mah gawd". Classic!
* This is like a video, only faster. Attitude Era is best, Stone Cold Steve Austin is too.
* I strived to meet all hard defined requirements, and showcase a little bit of everything without a hard requirement.
* I also added just a few Dusk - "headless browser" Tests as that's the latest and greatest for testing with Lararvel, the tip of the iceberg really.

I designed this to showcase a bit of Laravel 5.6 latest and greatest.

I've added a bit of everything to hit all bullet points/requirements with concern for my sanity and time.

I used Laravel - Valet on my local machine and I very much recommend it!

You can Register a User, Login, Upload, Add Metadata and Like Videos.

And that's the bottom line ;P

## Install

* Clone the [Repository](https://bitbucket.org/thecmachine/laravel-video-upload-app) into a VirtualServer Directory Locally or in your VM
```
$ git clone https://thecmachine@bitbucket.org/thecmachine/laravel-video-upload-app.git
```

* Composer 
```
$ composer install
```

* NPM 
```
$ npm install
```

* Yarn 
```
$ yarn run watch
```

Refresh and Seed Database for development and testing, I've committed the smallest video for development purposes
```
$ php artisan migrate:fresh --seed
```

* Edit .env DB_DATABASE, DB_USERNAME, and DB_PASSWORD Variables for your Local DB

## Testing

* Install Dusk on Dev Environment with artisan scaffolding
```
$ composer require laravel/dusk --dev
$ php artisan dusk:install
```

* Run Dusk Browser Tests/Test Suite
```
$ php artisan dusk 
```

Thanks a million for taking the time to explore my application, from a long time fan :)

 - Clint


