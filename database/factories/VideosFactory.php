<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Video::class, function (Faker $faker) {
    return [    
        'title' => $faker->word(),
	    'path' =>  "/videos/keep.mp4",
	    'file_size' => "1912914",
	    'duration' => "25.024", 
	    'format' => "mp4",
	    'bitrate' => "602761.5089514066", 
    ];
});
