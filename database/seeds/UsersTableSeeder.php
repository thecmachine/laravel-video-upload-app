<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$user = factory(\App\User::class, 10)->create();
		factory(\App\User::class)->create([
	        "name" => "clint",
	        "email" => "thecmachine05@gmail.com",
	        "password" => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
			"remember_token" => str_random(10),
			"created_at" =>  new DateTime(),
        ]); 
    }
}
