<?php

use Illuminate\Database\Seeder;

class MetadataTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//         $videos = factory(\App\Metadata::class, 3)->create();

		$videos = \App\Video::get();
		foreach($videos as $video){
			$locationMetadata = factory(\App\Metadata::class, 1)->create([
				"video_id" => $video->id,
				"video_title" => $video->title,
				"type" => "location",
			]);
			
			$locationMetadata = factory(\App\Metadata::class, 2)->create([
				"video_id" => $video->id,
				"video_title" => $video->title,				
				"type" => "keyword",
			]);
		}
    }
}
