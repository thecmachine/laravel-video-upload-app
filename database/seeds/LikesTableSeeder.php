<?php

use Illuminate\Database\Seeder;

class LikesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$videos = \App\Video::get();
		$users = \App\User::get();
		foreach($users as $user){
			foreach($videos as $video){
				//flip a coin OR true 50/50 to prepopulate Likes here
				$randomNumber = rand(0,1);
				if($randomNumber == 1){
					$newLike = factory(\App\Like::class, 1)->create([
						"video_id" => $video->id,
						"user_id" => $user->id,
					]);
				}
			}
		}
    }
}
