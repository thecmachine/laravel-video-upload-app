<?php

namespace App\Http\Controllers;

use App\Metadata;
use Illuminate\Http\Request;

class MetadataController extends Controller
{
	/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('metadata');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    
    /**
     * Get all Metadata
     *
     * @return \Illuminate\Http\Response
     */
    public function getAll()
    {
	 	$metadata = \App\Metadata::all();
	    return ['metadata' => $metadata];   
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		$video_id = $request->input('video_id');
		$type = $request->input('type');
		$value = $request->input('value');
		$video = \App\Video::where('id', $video_id)->first();
		
		//make sure only one location exists
		if($type == "location"){
			$metadata = \App\Metadata::where('id', $video_id)->where('type','location')->get();
			if(count($metadata) > 0){
				return array("message" => "Cannot save more than one location Metadata per video!");
			}else{
				//TODO abstract this, DRY stuff. infact I could probably delete it, will test when have time
				$newMetadata = new \App\Metadata;
				$newMetadata->video_id = $video_id;
				$newMetadata->video_title = $video->title;
				$newMetadata->type = $type;
				$newMetadata->value = $value;
				$newMetadata->save();
				return array("message" => "Saved Metadata Successfully!");				
			}
		}else{
			$newMetadata = new \App\Metadata;
			$newMetadata->video_id = $video_id;
			$newMetadata->video_title = $video->title;
			$newMetadata->type = $type;
			$newMetadata->value = $value;
			$newMetadata->save();
			return array("message" => "Saved Metadata Successfully!");								
		}
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Metadata  $metadata
     * @return \Illuminate\Http\Response
     */
    public function show(Metadata $metadata)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Metadata  $metadata
     * @return \Illuminate\Http\Response
     */
    public function edit(Metadata $metadata)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Metadata  $metadata
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Metadata $metadata)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Metadata  $metadata
     * @return \Illuminate\Http\Response
     */
    public function destroy(Metadata $metadata)
    {
        //
    }
}
