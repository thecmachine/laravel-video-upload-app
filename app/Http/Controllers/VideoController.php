<?php

namespace App\Http\Controllers;

use App\Video;
use Illuminate\Http\Request;
use getID3;

class VideoController extends Controller
{
	/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
	
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('videos');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function upload()
    {
        return view('upload');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		$this->validate($request, [
		    'title' => 'required',
		    'file' => 'required',
		]);       

		$getID3 = new getID3;
        $fileInfo = $getID3->analyze($request->file);
        
        if($fileInfo['fileformat'] != "mp4"){
	        return ['result' => false, "message" => "Video must be MP4!"];
        }

		$datetime = new \DateTime();
		$datetime = $datetime->format('YmdHis');

		$result = Video::create([
		    'title' => $request->title,
		    'path' =>  "/videos/" . $request->title . $datetime . ".mp4",
		    'file_size' => $fileInfo['filesize'],
		    'duration' => $fileInfo['playtime_seconds'], 
		    'format' => $fileInfo['fileformat'],
		    'bitrate' => $fileInfo['bitrate'], 
		]);
				
		if ($result) {
		    if ($request->hasFile('file') && $request->file('file')->isValid()) {
		        $request->file('file')->storeAs('public/videos', $request->title . $datetime . ".mp4");
		    }
		    return ['result' => true, "message" => "Saved Video"];
		} else {
		    return ['result' => false, "message" => "Unable to Save Video"];
		}
    }
   
    /**
     * Display the specified resource.
     *
     * @param  \App\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function show(Video $video)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function edit(Video $video)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Video $video)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function destroy(Video $video)
    {
        //
    }
    
    /**
     * Get all Videos.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function allVideos()
    {
		$videos = Video::leftJoin('likes', 'videos.id', '=', 'likes.video_id')->select('videos.*', \DB::raw('COUNT(`likes`.id) as likes' ) )->groupBy('videos.id')->get();
	    return ['videos' => $videos];
		
    }
}
