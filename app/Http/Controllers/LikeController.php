<?php

namespace App\Http\Controllers;

use App\Like;
use Illuminate\Http\Request;

class LikeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
	    $user_id =  \Auth::user()->id;
	    $video_id = $request->input('video_id');
	    
	    $existingLikes = \App\Like::where('video_id', $video_id)->where('user_id', $user_id)->get();
	    if(count($existingLikes) < 1){
		    $like = new \App\Like;
			$like->video_id = $video_id;
			$like->user_id = $user_id;
			$like->save();
			return array("message" => "Video Liked Successfully!");	
	    }else{
		 	$like = \App\Like::where('video_id', $video_id)->where('user_id', $user_id)->first();
		 	$like->delete();   
		    return array("message" => "Video Un-Liked Successfully!");	
	    }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Like  $like
     * @return \Illuminate\Http\Response
     */
    public function show(Like $like)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Like  $like
     * @return \Illuminate\Http\Response
     */
    public function edit(Like $like)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Like  $like
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Like $like)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Like  $like
     * @return \Illuminate\Http\Response
     */
    public function destroy(Like $like)
    {
        //
    }
    
}
