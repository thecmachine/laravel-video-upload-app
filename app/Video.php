<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    protected $fillable = ['title', 'path', 'duration', 'file_size', 'format', 'bitrate'];
}
